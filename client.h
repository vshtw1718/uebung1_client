//
// Created by Marc Szymkowiak
//

#ifndef CLIENT_CLIENT_H
#define CLIENT_CLIENT_H

void buildMessageString(int,int,char *[]);
int sendRequest(char[]);

#endif //CLIENT_CLIENT_H
