#include<stdlib.h>
#include<stdio.h>
#include <unistd.h>
#include<string.h>
#include<sys/socket.h>
#include<arpa/inet.h>

#include "client.h"

//global vars
int port;
char* ip;


int main(int argc, char *argv[]) {

    int byteSize, opt, numberOfFiles;
    char *files[5] = {};

    numberOfFiles = 0;

    if (argc < 9) {
        fprintf(stderr, "Usage: -l file1 [file2,..,file5] -n number of bytes -p port -i ip\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    while ((opt = getopt(argc, argv, "nlpi")) != -1) {
        switch (opt) {
            case 'n':
                byteSize = atoi(argv[optind]);
                if (byteSize > 10) {
                    perror("n darf maximal 10 sein \n");
                    return 1;
                }
                break;
            case 'l':
                for (; optind < argc && *argv[optind] != '-'; optind++) {
                    if (!(numberOfFiles < 5)) {
                        perror("Only five files are possible");
                        return 2;
                    }
                    files[numberOfFiles] = argv[optind];
                    numberOfFiles++;
                }
                break;
            case 'p':
                port = atoi(argv[optind]);
                if(!(port > 0 && port < 10000)) {
                    perror("Choose a port between 0 & 9999");
                    return 3;
                }
                break;
            case 'i':
                ip = argv[optind];
                break;
            default:
                fprintf(stderr, "Usage: -l file1 [file2,..,file5] -n number of bytes -p port -i ip\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }
    buildMessageString(byteSize, numberOfFiles, files);

}

/**
 * calculate full message size
 * @param byteSize number of bytes to read
 * @param numberOfFiles number of files to read
 * @param files array with filenames
 * @return
 */
int getFullSize(int byteSize, int numberOfFiles, char *files[]) {
    int fullSize = 0;
    //get char size of all files
    for (int fileIndex = 0; fileIndex < numberOfFiles; fileIndex++) {
        if (files[fileIndex] != 0) {
            fullSize += strlen(files[fileIndex]);
        }
    }

    //file1= (6 chars)+  & = 1 char * number of files + n=x
    fullSize += 6 * (numberOfFiles) + numberOfFiles + 4;

    //if numberOfBytes is 10 we need one more char
    if (byteSize == 10) {
        fullSize++;
    }

    return fullSize;
}

/**
 * build message String
 * @param byteSize number of bytes to read
 * @param numberOfFiles number of files to read
 * @param files array with filenames
 */
void buildMessageString(int byteSize, int numberOfFiles, char *files[]) {
    int fullSize = 0;
    char byteLenght[3];

    sprintf(byteLenght, "%d", byteSize);

    fullSize = getFullSize(byteSize, numberOfFiles, files);

    char *requestMessage;


    requestMessage = calloc(fullSize, sizeof(char));


    strcat(requestMessage, "n=");
    strcat(requestMessage, byteLenght);

    for (int fileNumber = 0; fileNumber < numberOfFiles; fileNumber++) {
        char fileN[2];
        sprintf(fileN, "%d", fileNumber);
        strcat(requestMessage, "&");
        strcat(requestMessage, "file");
        strcat(requestMessage, fileN);
        strcat(requestMessage, "=");
        strcat(requestMessage, files[fileNumber]);
    }

    sendRequest(requestMessage);
    free(requestMessage);
}

/**
 * Send Request to socket and read reply
 * @param requestMessage the message to send to the client
 * @return
 */
int sendRequest(char requestMessage[]) {
    int socketDescriptor;
    struct sockaddr_in server;
    char serverReply[2000];

    //Create socket
    socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketDescriptor == -1) {
        printf("Create socket failed");
    }
    puts("Socket created");

    server.sin_addr.s_addr = inet_addr(ip);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    //Connect to remote server
    if (connect(socketDescriptor, (struct sockaddr *) &server, sizeof(server)) < 0) {
        perror("failed to connect to remote server");
        return 1;
    }

    printf("Connected to remoteserver succesfully\n");
    printf("Send Message: %s\n", requestMessage);

    //Send some data
    if (send(socketDescriptor, requestMessage, strlen(requestMessage), 0) < 0) {
        puts("Send failed");
        return 1;
    }
    //Receive a reply from the server
    if (recv(socketDescriptor, serverReply, 2000, 0) < 0) {
        puts("Recv failed");
    }

    int count = 1;
    for (int i = 0; serverReply[i] != '\0'; i++) {
        if (serverReply[i] == '&')
            count++;
    }

    char *p = strtok(serverReply, "&");

    char *files[5] = {};
    int i = 0;
    while (p != NULL) {
        files[i++] = p;
        p = strtok(NULL, "&");
    }

    puts("Server reply :\n");
    for (int j = 0; j < count; j++) {
        printf("%s\n", files[j]);
    }


    close(socketDescriptor);
    return 0;

}

